<?php

class User extends Eloquent {

	public static $table = 'users';
	public static $timestamps = true;



/**
	 * Current loaded Project
	 *
	 * @var Project
	 */
	private static $current = null;

	/**
	* Return the current loaded Project object
	*
	* @return Project
	*/
	public static function current()
	{
		return static::$current;
	}

	/**
	* Load a new Project into $current, based on the $id
	*
	* @param   int  $id
	* @return  void
	*/
	public static function load_user($id)
	{
		static::$current = static::find($id);
	}


	/**********************************************************
	* Methods to use with loaded User
	**********************************************************/

	/**
	* Checks to see if $this user is current user
	*
	* @return bool
	*/
	public function me(){
		return $this->id == Auth::user()->id;
	}






	/**
	* Check to see if current user has permission to see project
	*
	* @param  int   $project_id
	* @return bool
	*/
	public function project_permission($project_id = null)
	{
		if(is_null($project_id))
		{
			$project_id = Project::current()->id;
		}

		if($this->permission('project-all'))
		{
			return true;
		}

		if(Project\User::check_assign($this->id, $project_id))
		{
			return true;
		}

		return false;
	}

	/**
	* Select all issues assigned to a user
	*
	* @param int $status
	* @return mixed
	*/
	public function issues($status = 1)
	{
		return $this->has_many('Project\Issue', 'created_by')
			->where('status', '=', 1)
			->where('assigned_to', '=', $this->id);
	}

	

	/******************************************************************
	* Static methods for working with users
	******************************************************************/

	/**
	* Update a user
	*
	* @param  array  $info
	* @param  int    $id
	* @return array
	*/
	public static function update_user($info, $id){
	
		$rules = array(
			'firstname' => array('required', 'max:50'),
			'lastname' => array('required', 'max:50'),
			'email' => array('required', 'email'),
		);

		/* Validate the password */
		if($info['password'])
		{
			$rules['password'] = 'confirmed';
		}

		$validator = Validator::make($info, $rules);

		if($validator->fails())
		{
			return array(
				'success' => false,
				'errors' => $validator->errors
			);
		}

		
		$update = array(
			'email' => $info['email'],
			'firstname' => $info['firstname'],
			'lastname' => $info['lastname'],
			'post'=>$info['post'],
			'phones' => $info['phones'],
			'vk'=>$info['vk'],
			'facebook'=>$info['facebook'],
			'linkedin'=>$info['linkedin'],
			'skype'=>$info['skype'],
			'role_id' => $info['role_id']
		);

		/* Update the password */
		if($info['password'])
		{
			$update['password'] = Hash::make($info['password']);
		}

		User::find($id)->fill($update)->save();

		return array(
			'success' => true
		);
		
	}

	/**
	* Add a new user
	*
	* @param  array  $info
	* @return array
	*/
	public static function add_user($info){
	
	
		$rules = array(
			'firstname' => array('required', 'max:50'),
			'lastname' => array('required', 'max:50'),
			'email' => array('required', 'email'),
		);

		$validator = Validator::make($info, $rules);

		if($validator->fails())
		{
			return array(
				'success' => false,
				'errors' => $validator->errors
			);
		}

		$role_id = 3;
		$admin = 0;
		if(!empty($info['admin'])){         
			$role_id = 1;
			$admin = 1;
			}
			
		
			
		
		$insert = array(
			'email' => $info['email'],
			'firstname' => $info['firstname'],
			'lastname' => $info['lastname'],
			'post'=>$info['post'],
			'role_id' => $role_id,
			'admin' => $admin,
			'password' => Hash::make($password = Str::random(6))
		);

		$user = new User;
		$user->fill($insert)->save();

		/* Send Activation email */
		$view = View::make('email.new_user', array(
			'email' => $info['email'],
			'password' => $password
		));


		

		$proj_id = 0;
		$expert = 0; 
		
		if(!empty($info['expert'])){  
		$expert = 1;						//expert || noexpert
		}
		
		$user_id = DB::table('users')->max('id');
		
		if(!empty($info['admin'])){         //ADMIN || CEO || spectator
			$role_id = 1;
			
	
		}
		
		if (!empty($info['ceotagsSelect'])) {
		
			$role_id = 2;
			$proj_id = $info['ceotagsSelect'];
				
			if(isset($proj_id)){
				foreach ($proj_id as $p_id){
					$item = new Project\User;
					$item-> fill(array(
					'user_id'=>$user_id,
					'project_id'=>$p_id,
					'role_id'=>$role_id,
					'expert'=>$expert
					));
					$item->save();
				}
			}
				
				
		} 
		
		
		if(!empty($info['spectatortagsSelect'])) {
		
			$role_id = 3;
			$proj_id = $info['spectatortagsSelect'];
		
				foreach ($proj_id as $p_id){
				
					$item = new Project\User;
					$item-> fill(array(
					'user_id'=>$user_id,
					'project_id'=>$p_id,
					'role_id'=>$role_id,
					'expert'=>$expert
					));
					$item->save();
				}
		
		}
		
		
		
		return array(
			'success' => true,
			'password' => $password
		);
		
		
		
		
		
		
	}

	/**
	* Soft deletes a user and empties the email
	*
	* @param  int   $id
	* @return bool
	*/
	public static function delete_user($id){
		$update = array(
			'email' => '',
			'deleted' => 1
		);

		User::find($id)->fill($update)->save();
		Project\User::where('user_id', '=', $id)->delete();

		return true;
	}


	public function user_rate($id){
		
		$count = User\Rate::where('cur_user_id','=',$id)->count();
								if ($count != 0)
								{
									$results = User\Rate::where('cur_user_id','=',$id)->get();
									$res = 0;
									foreach ($results as $r)
									{
										$res = $res + $r->rate;
									}
									return $res/$count;
								}
		
	}
	
	public function rate($rate,$cur_user_id){
		$result = \User\Rate::where('cur_user_id','=',$cur_user_id)->where('user_id','=',\Auth::user()->id);
		if($result->count()==0){
			$obj = new \User\Rate;
			$obj ->fill(array(
				'user_id'=>\Auth::user()->id,
				'cur_user_id'=>$cur_user_id,
				'master'=>0,
				'rate'=>$rate));
			$obj->save();
		}else {
			$first=$result->get();
			$obj = $first[0];
			$obj ->fill(array(
			
				'rate'=>$rate));
			$obj->save();	
		}
	}
	
	
		
	public function to($URL = ''){
		return \URL::to('team/' . $this->id . (($URL) ? '/'. $URL : ''));
	}
	
	
	
	
	
}